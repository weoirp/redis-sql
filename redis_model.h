#pragma once

#include "db_model.h"


/**
 * SQL Map to Redis
 * 
 * prikey   =>  pri1=val1,pri2=val2,...
 * mulkey   =>  mul1=val1,mul2=val2,...
 * 
 * 
 * A row data
 * key      =>  id = [table:pri]
 * value    =>  {field1:value1, field2:value2, field3:value3, ..., }
 * 
 * key      =>  [table:tag]
 * value    =>  [mul1:val1, mul1:val2, mul2:val1, mul2:val3, mul3:val2, ...]
 * 
 * 
 * key      =>  [table:mul]
 * value    =>  [id1, id2, id3]
*/

class RedisModel : public DBModel
{
public:
    bool Query(DBRecord *record);
    bool Update(DBRecord *record);
    bool Insert(DBRecord *record);
    bool Delete(DBRecord *record); 
};


class RedisResult : public DBResult
{
public:
    RedisResult(DBFieldInfoListPtr info, size_t size=0);
    ~RedisResult();

    size_t GetNumRow() const ;
    DBRecordPtr FetchRow(bool can_modify = true);

    bool Add(DBRecord *db_record);

private:

    DBFieldInfoListPtr m_db_field_info;
    std::vector<DBRecord *> m_result;
    std::vector<DBRecord *>::iterator m_iter;

    size_t m_num_rows;
};