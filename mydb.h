#pragma once

#include <memory>

#include "db_connection.h"
#include "db_model.h"
#include "db_record.h"

using CacheDBPtr = std::unique_ptr<DBConnection>;
using StoreDBPtr = std::unique_ptr<DBConnection>;

class MyDBOper : public DBOper
{
public:

    bool Query(DBRecord *record);
    bool Insert(DBRecord *record);
    bool Update(DBRecord *record);
    bool Delete(DBRecord *record);

private:
    CacheDBPtr cache_ptr;
    StoreDBPtr store_ptr;
};

class MyDBConnection : public DBConnection
{
public:
    DBResult Query(const std::string &query);

private:
    bool TryQueryFromCache(const std::string &query);
    bool QueryFromStore(const std::string &query);

    CacheDBPtr cache_ptr;
    StoreDBPtr store_ptr;    
};