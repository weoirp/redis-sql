#include "redis_model.h"

bool RedisModel::Query(DBRecord *record)
{

}

bool RedisModel::Update(DBRecord *record)
{

}

bool RedisModel::Insert(DBRecord *record)
{

}

bool RedisModel::Delete(DBRecord *record) 
{

}





RedisResult::RedisResult(DBFieldInfoListPtr info, size_t size/*=0*/)
    :m_num_rows(0)
{
    m_result.resize(size);
    m_iter = m_result.begin();
}

RedisResult::~RedisResult()
{

}

size_t RedisResult::GetNumRow() const
{
    return m_num_rows;
}

DBRecordPtr RedisResult::FetchRow(bool can_modify /*= true*/) 
{
    return DBRecordPtr(*(m_iter++));
}

bool RedisResult::Add(DBRecord *db_record) 
{
    m_result.push_back(db_record);

    ++m_num_rows;
    return true;
}