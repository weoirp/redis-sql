#pragma once

#include "db_connection.h"

class DBResult;
class MySQLConnection : public DBConnection
{
public:
    DBResult Query(const std::string &query);
    bool Update(const std::string &query);
    bool Insert(const std::string &query);
    bool Delete(const std::string &query);

};

