#pragma once

#include <map>
#include <memory>
#include <string>
#include <variant>

enum DB_FIELD_TYPES
{
	DB_FIELD_TYPE_UNSUPPORED,
	DB_FIELD_TYPE_TINY,				// -128 ~ 127
	DB_FIELD_TYPE_TINY_UNSIGNED,	// 0 ~ 255
	DB_FIELD_TYPE_SHORT,			// -32768 ~ 32767
	DB_FIELD_TYPE_SHORT_UNSIGNED,	// 0 ~ 65535
	DB_FIELD_TYPE_LONG,				// -2147483648 ~ 2147483647
	DB_FIELD_TYPE_LONG_UNSIGNED,	// 0 ~ 4294967295
	DB_FIELD_TYPE_LONGLONG,			// -2^63 ~ 2^63-1
	DB_FIELD_TYPE_LONGLONG_UNSIGNED,// 0 ~ 2^64
	DB_FIELD_TYPE_FLOAT,
	DB_FIELD_TYPE_DOUBLE,
	DB_FIELD_TYPE_VARCHAR,
};

enum DB_KEY_TYPES
{
	DB_NOT,
    DB_PRI,
    DB_UNI,
    DB_MUL,
};

struct DBFieldInfo
{
	DBFieldInfo() {}
	virtual ~DBFieldInfo() {}


	virtual const char* get_table_name() const = 0;
	virtual const char* get_field_name() const = 0;
	virtual DB_FIELD_TYPES get_field_type() const = 0;
	virtual DB_KEY_TYPES get_key_type() const = 0;
};

class DBFieldInfoList
{
public:
	DBFieldInfoList() {}
	virtual ~DBFieldInfoList() {}
	virtual const DBFieldInfo* operator[](size_t idx)const = 0;
	virtual const DBFieldInfo* get(size_t idx)const = 0;
	virtual size_t size() const = 0;
private:
	std::string m_table_name;
};
using DBFieldInfoListPtr = std::shared_ptr<DBFieldInfoList>;


class DBField
{
public:
    DBField();
    ~DBField();

    bool IsSelected() const;
    bool CanModify() const;
    bool IsModified() const;
    void SetModified();


private:
    bool m_selected;
    bool m_modified;

	std::variant<int8_t, uint8_t, int16_t, uint16_t, int32_t, uint32_t, int64_t, uint64_t, float, double, std::string> m_val;
};
