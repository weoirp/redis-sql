#pragma once

#include <string>

#include "db_field.h"
#include "db_record.h"

//like a relation db's table
class DBModel
{
public:
    virtual bool Query(DBRecord *record) = 0;
    virtual bool Update(DBRecord *record) = 0;
    virtual bool Insert(DBRecord *record) = 0;
    virtual bool Delete(DBRecord *record) = 0;
private:
    std::string model_name;
    DBFieldInfoListPtr model_info;
};


//create and save db model
class DBModelManager
{
public:
    /**
     * model = {"mysql", "redis"}
    */
    DBModel *CreateModel(const std::string &model, DBFieldInfoListPtr model_info);
    
};