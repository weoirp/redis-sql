#pragma once

#include <string>

#include "db_record.h"

class DBConnection
{
public:
    virtual DBResult Query(const std::string &query) = 0;
    virtual bool Update(const std::string &query) = 0;
    virtual bool Insert(const std::string &query) = 0;
    virtual bool Delete(const std::string &query) = 0;
private:
};

class DBOper
{
public:
    virtual bool Query(DBRecord *record) = 0;
    virtual bool Insert(DBRecord *record) = 0;
    virtual bool Update(DBRecord *record) = 0;
    virtual bool Delete(DBRecord *record) = 0;
};