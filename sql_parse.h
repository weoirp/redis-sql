#pragma once

#include <string>

#include "db_field.h"

class SQLParse
{
public:
    virtual DBFieldInfoListPtr ParseCreateSQL(const std::string &sql) = 0;
private:
};


class MySQLParse : public SQLParse
{
public:
    DBFieldInfoListPtr ParseCreateSQL(const std::string &sql);
private:
};