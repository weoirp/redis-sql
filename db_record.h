#pragma once

#include <vector>
#include <unordered_map>

#include "db_field.h"

class DBOper;

//like a relation db's row
class DBRecord
{
public:
    DBRecord();
    ~DBRecord();

    DBField &Field(size_t idx);
    DBField &Field(const std::string &name);
    
    bool GetCanModified() const 
    {
        return m_can_modified;
    }
    bool GetModified() const
    {
        return m_is_modified;
    }
    void SetModified(bool value)
    {
        m_is_modified = value;
    }

    bool UpdateRecord();
    bool DeleteRecord();

private:
    std::vector<DBField *> m_fields_by_idx;
    std::unordered_map<std::string, DBField *> m_fields_by_name;

    DBOper *db_oper;
    DBFieldInfoListPtr m_db_field_info;

    bool m_can_modified;
    bool m_is_modified;
};

using DBRecordPtr = std::unique_ptr<DBRecord>;


class DBResult
{
public:
    virtual size_t GetNumRow() const = 0;
    virtual DBRecordPtr FetchRow(bool can_modify = true) = 0;
    virtual DBFieldInfoListPtr GetFieldInfoList() const = 0;
}