#pragma once

#include "db_record.h"
#include "db_model.h"


class MySQLModel : public DBModel
{
public:
    bool Query(DBRecord *record);
    bool Update(DBRecord *record);
    bool Insert(DBRecord *record);
    bool Delete(DBRecord *record);

};


class MySQLResult : public DBResult
{
public:
    MySQLResult();
    ~MySQLResult();

    size_t GetNumRow() const;
    DBRecordPtr FetchRow(bool can_modify = true);

private:
    DBFieldInfoListPtr m_db_field_info;
};

